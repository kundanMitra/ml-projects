#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 10:12:14 2020

@author: kundanmitra
"""


import warnings

warnings.filterwarnings('ignore')

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import chi2
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

sns.set(rc={'figure.figsize':(15,12)})

df = pd.read_csv('train.csv')
df.head()


# Data Cleaning and Preparing 
df["category"] = df[['Computer Science', 'Physics', 'Mathematics',
       'Statistics', 'Quantitative Biology', 'Quantitative Finance']].idxmax(axis=1)

df.drop(['ID','Computer Science', 'Physics', 'Mathematics',
       'Statistics', 'Quantitative Biology', 'Quantitative Finance'], axis=1, inplace=True)
df.head()

validation_df = pd.read_csv('test.csv')
validation_df.head()

sns.countplot(x ='category', data = df)
sns.heatmap(df.isnull(), cbar=False)

df['TITLE'] = df['TITLE'].str.replace("\n", " ")
df['TITLE'] = df['TITLE'].str.replace("\r", " ")
df['TITLE'] = df['TITLE'].str.replace("\t", " ")
df['TITLE'] = df['TITLE'].str.replace("\\", "")
df['TITLE'] = df['TITLE'].str.replace('"', '')
df['TITLE'] = df['TITLE'].str.replace('   ', '')
df['TITLE'] = df['TITLE'].str.lower()
df['TITLE'] = df['TITLE'].str.replace("'s", "")

df['ABSTRACT'] = df['ABSTRACT'].str.replace("\n", " ")
df['ABSTRACT'] = df['ABSTRACT'].str.replace("\r", " ")
df['ABSTRACT'] = df['ABSTRACT'].str.replace("\t", " ")
df['ABSTRACT'] = df['ABSTRACT'].str.replace("\\", "")
df['ABSTRACT'] = df['ABSTRACT'].str.replace('"', '')
df['ABSTRACT'] = df['ABSTRACT'].str.lower()
df['ABSTRACT'] = df['ABSTRACT'].str.replace('   ', '')
df['ABSTRACT'] = df['ABSTRACT'].str.replace("'s", "")

punctuation_signs = list("?:!.,;-{}()[]\\\'$´-_=ζ£#@/`^*+")
df['TITLE'] = df['TITLE']
for punct_sign in punctuation_signs:
    df['TITLE'] = df['TITLE'].str.replace(punct_sign, '')
    df['ABSTRACT'] = df['ABSTRACT'].str.replace(punct_sign, '')
    
#nltk.download('punkt')
#nltk.download('wordnet')

wordnet_lemmatizer = WordNetLemmatizer()
nrows = len(df)
lemmatized_text_list = []

for row in range(0, nrows):
    
    # Create an empty list containing lemmatized words
    lemmatized_list = []
    
    # Save the text and its words into an object
    text = df.loc[row]['TITLE'] + df.loc[row]['ABSTRACT']
    text_words = text.split(" ")

    # Iterate through every word to lemmatize
    for word in text_words:
        lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos="v"))
        
    # Join the list
    lemmatized_text = " ".join(lemmatized_list)
    
    # Append to the list containing the texts
    lemmatized_text_list.append(lemmatized_text)
    
df['Content_lemmitized'] = lemmatized_text_list

#nltk.download('stopwords')
stop_words = list(stopwords.words('english'))

df['Words'] = df['Content_lemmitized']

for stop_word in stop_words:

    regex_stopword = r"\b" + stop_word + r"\b"
    df['Words'] = df['Words'].str.replace(regex_stopword, '')

#print(len(df.loc[5]['Words']), len(df.loc[5]['Content_lemmitized']))

print(np.unique(df['category']))

relValue = {
    'Computer Science':0,
    'Mathematics':1,
    'Physics':2,
    'Quantitative Biology':3,
    'Quantitative Finance':4,
    'Statistics':5
}

df["category"] = df['category'].map(relValue).astype(int)

X_train, X_test, y_train, y_test = train_test_split(df['Words'], 
          df['category'], test_size=0.2, random_state=42, stratify=df['category'])

# TFIDF
# Parameter election
ngram_range = (1,2)
min_df = 10
max_df = 1.0
max_features = 300

tfidf = TfidfVectorizer(encoding='utf-8',
                        ngram_range=ngram_range,
                        stop_words=None,
                        lowercase=False,
                        max_df=max_df,
                        min_df=min_df,
                        max_features=max_features,
                        norm='l2',
                        sublinear_tf=True)
                        
features_train = tfidf.fit_transform(X_train).toarray()
labels_train = y_train
print(features_train.shape)

features_test = tfidf.transform(X_test).toarray()
labels_test = y_test
print(features_test.shape)

from sklearn.ensemble import GradientBoostingClassifier
from pprint import pprint
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import ShuffleSplit

print(features_train.shape)
print(features_test.shape)

gb_0 = GradientBoostingClassifier(random_state = 8)

print('Parameters currently in use:\n')
pprint(gb_0.get_params())

# n_estimators
n_estimators = [200, 800]

# max_features
max_features = ['auto', 'sqrt']

# max_depth
max_depth = [10, 40]
max_depth.append(None)

# min_samples_split
min_samples_split = [10, 30, 50]

# min_samples_leaf
min_samples_leaf = [1, 2, 4]

# learning rate
learning_rate = [.1, .5]

# subsample
subsample = [.5, 1.]

# Create the random grid
random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'learning_rate': learning_rate,
               'subsample': subsample}

pprint(random_grid)

# First create the base model to tune
gbc = GradientBoostingClassifier(random_state=8)

# Definition of the random search
random_search = RandomizedSearchCV(estimator=gbc,
                                   param_distributions=random_grid,
                                   n_iter=50,
                                   scoring='accuracy',
                                   cv=3, 
                                   verbose=1, 
                                   random_state=8)

# Fit the random search model
random_search.fit(features_train, labels_train)

print("The best hyperparameters from Random Search are:")
print(random_search.best_params_)
print("")
print("The mean accuracy of a model with these hyperparameters is:")
print(random_search.best_score_)